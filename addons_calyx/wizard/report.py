# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from importlib.resources import contents
from odoo import _, api, fields, models
from odoo.exceptions import ValidationError

class ReportWizard(models.TransientModel):
    _name = 'addons_calyx.report'

    date_start = fields.Date(string="From",required=True)
    date_end = fields.Date(string="To",required=True)


    def create_pdf(self):
        search = self.env['addons_calyx.register.floaing'].search([
            ('date_admission', '>=', self.date_start),
            ('date_admission', '<=', self.date_end),
        ])
        if search:
            values = []
            for data in search:
                values.append({
                    'km':data.km,
                    'opening_balance':data.opening_balance,
                    'brand_id':data.brand_id.name,
                    'maintenance':data.maintenance,
                    'date_admission':data.date_admission,
                    'license_plate':data.license_plate,
                    'model_vehicle':data.model_vehicle
                })

            contents = {
                'values':values,
                'from':self.date_start,
                'to':self.date_end,
            }
            return self.env.ref('addons_calyx.action_report_register_floaing').report_action(self,data=contents)
            



    
  