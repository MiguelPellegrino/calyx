from odoo import models, fields, api

class Brand(models.Model):
    _name = 'addons_calyx.brand'
    _rec_name = 'name'

    name = fields.Char(string="Name brand")
    
