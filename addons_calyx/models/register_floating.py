from calendar import month
from odoo import models, fields, api
from datetime import timedelta, datetime
from dateutil.relativedelta import relativedelta

class RegisterFloating(models.Model):
    _name = 'addons_calyx.register.floaing'

    km = fields.Float(string="KM")
    license_plate = fields.Char(string="License Plate")
    model_vehicle = fields.Char(string="Model Vehicle")
    opening_balance = fields.Float(string="Opening Balance")
    brand_id = fields.Many2one('addons_calyx.brand', string="Brand")
    maintenance = fields.Integer(string="Number of maintenance",readonly=True)
    date_admission = fields.Date(string="Date of admission")

    @api.depends('km')
    @api.onchange('km')
    def _onchange_opening_balance(self):
        if self.km:
            tasa = int(round(self.km/10000,0))
            self.opening_balance = self.opening_balance * (0.05 * tasa)
    
    @api.depends('date_admission')
    @api.onchange('date_admission')
    def _onchange_opening_date_admission(self):
        if self.date_admission:
            count = 0
            date = self.date_admission
            while date + relativedelta(months=6) < datetime.now().date():
                count += 1
                date = date + relativedelta(months=6)

            self.maintenance = count
    
